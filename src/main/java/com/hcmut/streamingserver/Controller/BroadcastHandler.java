package com.hcmut.streamingserver.Controller;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.util.DateTime;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.*;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Date;
import java.util.List;

@Service
public class BroadcastHandler {

    public static List<String> scopes = Lists.newArrayList("https://www.googleapis.com/auth/youtube");

    LiveStream createStream(String title) throws IOException {
        Credential credential = Auth.authorize(scopes, "all");
        YouTube youtube = new YouTube.Builder(Auth.HTTP_TRANSPORT, Auth.JSON_FACTORY, credential)
                .setApplicationName("VideoUpload").build();
        ;

        LiveStreamSnippet streamSnippet = new LiveStreamSnippet();
        streamSnippet.setTitle(title);
        CdnSettings cdnSettings = new CdnSettings();
        cdnSettings.setFormat("1080p");
        cdnSettings.setIngestionType("rtmp");

        LiveStream stream = new LiveStream();
        stream.setKind("youtube#liveStream");
        stream.setSnippet(streamSnippet);
        stream.setCdn(cdnSettings);

        // Construct and execute the API request to insert the stream.
        YouTube.LiveStreams.Insert liveStreamInsert
                = youtube.liveStreams().insert("status,snippet,cdn", stream);
        LiveStream returnedStream = liveStreamInsert.execute();
        return returnedStream;
    }

    String createBroadcast(String title) throws IOException {
        Credential credential = Auth.authorize(scopes, "all");
        YouTube youtube = new YouTube.Builder(Auth.HTTP_TRANSPORT, Auth.JSON_FACTORY, credential)
                .setApplicationName("VideoUpload").build();

        Date currentDate = new Date();

//        String title = title + " " + currentDate;
        LiveBroadcastSnippet broadcastSnippet = new LiveBroadcastSnippet();
        broadcastSnippet.setTitle(title + " " + currentDate);
        broadcastSnippet.setScheduledStartTime(new DateTime(currentDate));

        LiveBroadcastStatus status = new LiveBroadcastStatus();
        status.setPrivacyStatus("public");

        LiveBroadcastContentDetails contentDetails = new LiveBroadcastContentDetails();
        MonitorStreamInfo monitorStreamInfo = new MonitorStreamInfo();
        monitorStreamInfo.setEnableMonitorStream(false);
        contentDetails.setMonitorStream(monitorStreamInfo);

        LiveBroadcast broadcast = new LiveBroadcast();
        broadcast.setKind("youtube#liveBroadcast");
        broadcast.setSnippet(broadcastSnippet);
        broadcast.setStatus(status);
        broadcast.setContentDetails(contentDetails);

        YouTube.LiveBroadcasts.Insert liveBroadcastInsert = youtube.liveBroadcasts().insert("snippet,status,contentDetails", broadcast);
        LiveBroadcast returnedBroadcast = liveBroadcastInsert.execute();

        LiveStream stream = this.createStream(title + " " + currentDate);

        YouTube.LiveBroadcasts.Bind liveBroadcastBind
                = youtube.liveBroadcasts().bind(returnedBroadcast.getId(), "id,status,contentDetails");
        liveBroadcastBind.setStreamId(stream.getId());
        returnedBroadcast = liveBroadcastBind.execute();

        BroadcastThread broadcastThread = new BroadcastThread(returnedBroadcast, stream, youtube);
        Thread thread = new Thread(broadcastThread);
        thread.start();

        String broadcastLink = stream.getCdn().getIngestionInfo().getStreamName() + "/" + returnedBroadcast.getId();
        System.out.println("broadcastLink: " + broadcastLink);
        return broadcastLink;
    }

    void closeBroadcast(String id) throws IOException {
        Credential credential = Auth.authorize(scopes, "all");
        YouTube youtube = new YouTube.Builder(Auth.HTTP_TRANSPORT, Auth.JSON_FACTORY, credential)
                .setApplicationName("VideoUpload").build();
        try {
            YouTube.LiveBroadcasts.Transition transitionRequest = youtube.liveBroadcasts().transition("complete", id, "status");
            transitionRequest.execute();
        } catch (Exception e) {
            System.err.println(e);
        }

    }
}
