package com.hcmut.streamingserver.Controller;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.googleapis.media.MediaHttpUploader;
import com.google.api.client.googleapis.media.MediaHttpUploaderProgressListener;
import com.google.api.client.http.InputStreamContent;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.Video;
import com.google.api.services.youtube.model.VideoSnippet;
import com.google.api.services.youtube.model.VideoStatus;
import com.google.common.collect.Lists;
import com.hcmut.streamingserver.Controller.Auth;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class UploadHandler {

    private static YouTube youtube;
    private static final String VIDEO_FILE_FORMAT = "video/*";

    HashMap<String, String> uploadVideo(String title, String path1, String path2) {

        HashMap<String, String> response = new HashMap<>();
        String returnStatus = "Failed";
        String returnMessage;

        List<String> scopes = Lists.newArrayList("https://www.googleapis.com/auth/youtube.upload");

        try {
            Credential credential = Auth.authorize(scopes, "all");

            youtube = new YouTube.Builder(Auth.HTTP_TRANSPORT, Auth.JSON_FACTORY, credential).setApplicationName(
                    "VideoUpload").build();

            // Video 1

            Video videoObjectDefiningMetadata1 = new Video();

            VideoStatus status1 = new VideoStatus();
            status1.setPrivacyStatus("public");
            videoObjectDefiningMetadata1.setStatus(status1);

            VideoSnippet snippet1 = new VideoSnippet();

            Date currentDate = new Date();
            snippet1.setTitle(title + " " + currentDate);
            snippet1.setDescription("Upload " + title + " on " + currentDate);

            videoObjectDefiningMetadata1.setSnippet(snippet1);
            InputStream inputStream1 = new FileInputStream(path1);
            InputStreamContent mediaContent1 = new InputStreamContent(VIDEO_FILE_FORMAT, inputStream1);

            YouTube.Videos.Insert videoInsert1 = youtube.videos()
                    .insert("snippet,statistics,status", videoObjectDefiningMetadata1, mediaContent1);

            MediaHttpUploader uploader1 = videoInsert1.getMediaHttpUploader();
//            uploader1.setDirectUploadEnabled(true);

            Video returnedVideo1 = videoInsert1.execute();

            System.out.println("\n================== Returned Video ==================\n");
            System.out.println("  - Id: " + returnedVideo1.getId());
            System.out.println("  - Title: " + returnedVideo1.getSnippet().getTitle());
            System.out.println("  - Tags: " + returnedVideo1.getSnippet().getTags());
            System.out.println("  - Privacy Status: " + returnedVideo1.getStatus().getPrivacyStatus());
            System.out.println("  - Video Count: " + returnedVideo1.getStatistics().getViewCount());

            // Video 2

            Video videoObjectDefiningMetadata2 = new Video();

            VideoStatus status2 = new VideoStatus();
            status2.setPrivacyStatus("public");
            videoObjectDefiningMetadata2.setStatus(status2);

            VideoSnippet snippet2 = new VideoSnippet();

            snippet2.setTitle(title + " " + currentDate);
            snippet2.setDescription("Upload " + title + " on " + currentDate);

            videoObjectDefiningMetadata2.setSnippet(snippet2);
            InputStream inputStream2 = new FileInputStream(path2);
            InputStreamContent mediaContent2 = new InputStreamContent(VIDEO_FILE_FORMAT, inputStream2);

            YouTube.Videos.Insert videoInsert2 = youtube.videos()
                    .insert("snippet,statistics,status", videoObjectDefiningMetadata2, mediaContent2);

            MediaHttpUploader uploader2 = videoInsert2.getMediaHttpUploader();
//            uploader.setDirectUploadEnabled(true);

            Video returnedVideo2 = videoInsert2.execute();


            System.out.println("\n================== Returned Video ==================\n");
            System.out.println("  - Id: " + returnedVideo2.getId());
            System.out.println("  - Title: " + returnedVideo2.getSnippet().getTitle());
            System.out.println("  - Tags: " + returnedVideo2.getSnippet().getTags());
            System.out.println("  - Privacy Status: " + returnedVideo2.getStatus().getPrivacyStatus());
            System.out.println("  - Video Count: " + returnedVideo2.getStatistics().getViewCount());

            returnStatus = "OK";
            response.put("id1", returnedVideo1.getId());
            response.put("id2", returnedVideo2.getId());
        }  catch (GoogleJsonResponseException e) {
            returnMessage = "GoogleJsonResponseException code: " + e.getDetails().getCode() + " : "
                    + e.getDetails().getMessage();
            response.put("message", returnMessage);
            e.printStackTrace();
        } catch (IOException e) {
            returnMessage = "IOException: " + e.getMessage();
            response.put("message", returnMessage);
            e.printStackTrace();
        } catch (Throwable t) {
            returnMessage = "Throwable: " + t.getMessage();
            response.put("message", returnMessage);
            t.printStackTrace();
        }

        response.put("status", returnStatus);
        return response;
    }

}
