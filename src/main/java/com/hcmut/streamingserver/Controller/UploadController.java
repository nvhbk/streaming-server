package com.hcmut.streamingserver.Controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
@RequestMapping("/upload")
public class UploadController {

    private UploadHandler handler = new UploadHandler();

    @RequestMapping(value = "/new", method = RequestMethod.GET, produces = "application/json")
    public HashMap<String, String> uploadVideo(@RequestParam String title, @RequestParam String path1, @RequestParam String path2) {
        System.out.println(title);
        System.out.println(path1);
        System.out.println(path2);
        return handler.uploadVideo(title, path1, path2);
    }
}
