package com.hcmut.streamingserver.Controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping("/api")
public class BroadcastController {

    private BroadcastHandler handler = new BroadcastHandler();

    @RequestMapping(value = "/{title}", method = RequestMethod.GET)
    public ResponseEntity<?> createBroadcast(@PathVariable(value = "title") String title) throws IOException {
        return new ResponseEntity<String>(handler.createBroadcast(title), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteBroadcast(@PathVariable(value = "id") String id) throws IOException {
        handler.closeBroadcast(id);
        return new ResponseEntity<String>("Success!", HttpStatus.OK);
    }
}
