package com.hcmut.streamingserver.Controller;

import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.LiveBroadcast;
import com.google.api.services.youtube.model.LiveStream;
import com.google.api.services.youtube.model.LiveStreamListResponse;

import java.io.IOException;

public class BroadcastThread implements Runnable {
    private LiveBroadcast broadcast;
    private YouTube youTube;
    private LiveStream stream;
    public BroadcastThread (LiveBroadcast broadcast, LiveStream stream, YouTube youtube) {
        this.broadcast = broadcast;
        this.stream = stream;
        this.youTube = youtube;
    }
    public void run() {
        int i = 0;
        while (i < 12) {
            try {
                if (!stream.getStatus().getStreamStatus().equals("active")) {
                    Thread.sleep(10000);
                } else {
                    YouTube.LiveBroadcasts.Transition transitionRequest = youTube.liveBroadcasts().transition("live", broadcast.getId(), "status");
                    broadcast = transitionRequest.execute();
                    if (broadcast.getStatus().getLifeCycleStatus().equals("liveStarting"))
                        break;
                    else
                        Thread.sleep(60000);
                }
                YouTube.LiveStreams.List livestreamRequest = youTube.liveStreams().list("id,status");
                livestreamRequest.setId(stream.getId());
                LiveStreamListResponse list = livestreamRequest.execute();
                stream = list.getItems().get(0);
                i++;
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
