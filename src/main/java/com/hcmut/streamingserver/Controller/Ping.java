package com.hcmut.streamingserver.Controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ping")
public class Ping {
    @RequestMapping(value = "/tok", method = RequestMethod.GET)
    public String checkPing() {
        return "OK";
    }
}
